#Paco Alcantara

#importar sqlite3
BASE_DE_DATOS = "inventario.db"

def obtener_conexion():
 return sqlite3. conectar(BASE_DE_DATOS)


def crear_tablas():
    tablas = [
        """
 CREAR TABLA SI NO EXISTE inventario(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
 producto TEXTO NO NULO,
 descripcion TEXTO NO NULO
        );
        """
    ]
 conexión = obtener_conexion()
 cursor = conexión. cursor()
 para tabla en tablas:
 cursor. ejecutar(tabla)


def principal():
    crear_tablas()
    menú = """
1) Agregar nuevo producto
2) Editar producto existente
3) Eliminar producto existente
4) Ver listado de productos
5) Buscar descripcion de producto
6) Salir
Elige: """
    eleccion = ""
    while eleccion != "6":
 eleccion = entrada(menú)
        if eleccion == "1":
            producto = input("\nIngresa producto: ")
            # Comprobar si no existe
            posible_descripcion = buscar_descripcion_producto(producto)
 si posible_descripcion:
                print(f"El producto'{producto}' ya existe")
            else:
 descripcion= entrada("Ingresa la descripcion: ")
                agregar_producto(producto, descripcion)
                print("Producto agregado")
        if eleccion == "2":
            producto = input("\nIngresa el producto que quieres editar: ")
            nueva_descripcion = input("Ingresa la descripcion: ")
            editar_producto(producto, nueva_descripcion)
            print("Producto actualizado")
 si eleccion == "3":
 producto = input( "\nIngresa el producto a eliminar: ")
            eliminar_producto(producto)
 si eleccion == "4":
            productos = obtener_productos()
 print("\n======== Lista de productos ========\n")
 para productos en productos:
                # Al leer desde la base de datos se devuelven los datos como arreglo, por
                # lo que hay que imprimir el primer elemento
                print(producto[0])
 si eleccion == "5":
 producto = insumo(
                "\nIngresa el producto el cual quieres saber el descripcion: ")
            descripcion = buscar_descripcion_producto(producto)
 si descripcion:
                print(f"La descripción de '{producto}' es:\n{descripcion[0]}")
            else:
                print(f"Producto '{producto}' no encontrada")


def agregar_producto(producto, descripcion):
 conexión = obtener_conexion()
 cursor = conexión. cursor()
    sentencia = "INSERT INTO inventario(producto, descripcion) VALUES (?, ?)"
 cursor. execute(sentencia, [producto, descripcion])
 conexión. comprometerse()


def editar_producto(producto, nueva_descripcion):
 conexión = obtener_conexion()
 cursor = conexión. cursor()
 sentencia = "UPDATE inventario SET descripción = ? DONDE producto = ?"
 cursor. execute(sentencia, [nueva_descripcion, producto])
 conexión. comprometerse()


def eliminar_producto(producto):
 conexión = obtener_conexion()
 cursor = conexión. cursor()
 sentencia = "BORRAR DE INVENTARIO DONDE producto = ?"
 cursor. execute(sentencia, [producto])
 conexión. comprometerse()


def obtener_productos():
 conexión = obtener_conexion()
 cursor = conexión. cursor()
 consulta = "SELECT PRODUCTO DE inventario"
 cursor. ejecutar(consulta)
 volver cursor. fetchall()


def buscar_descripcion_producto(producto):
 conexión = obtener_conexion()
 cursor = conexión. cursor()
 consulta = 